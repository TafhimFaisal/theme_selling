<?php
$title = "SMO Service";
include '../layout/_header.php';
?>

    <?php include '../layout/navbar.php'?>

        
        <div class="container sarvicetable">
                <h1>
                    SMO Service pakage 
                </h1>
                <table>
                <tr>
                        <th>Item</th>
                        <th>Basic</th>
                        <th>Silver</th>
                        <th>Gold</th>
                        <th>Ploatinum</th>					
                    </tr>
                    <tr>
                        <th>Time Standard Pricing	<br>3 Months</th>
                        <th>USD 250/Month	<br>3 Months</th>
                        <th>USD 350/Month	<br>3 Months</th>
                        <th>USD 450/Month	<br>6 Months</th>
                        <th>USD 650/Month</th>
                    </tr>
                    <tr>
                        <th class="divid" colspan="5">Facebook Marketing</th> 
                    </tr>
                    <tr>
                        <th>FB Page Creation</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>FB Regular Updates</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>FB Page Fans/Likes/Subcribers</th>
                        <td>25$ Per 1k <br>(Per Month)</td>
                        <td>25$ Per 1k <br>(Per Month)</td>
                        <td>25$ Per 1k <br>(Per Month)</td>
                        <td>2.5K Free<br>(Per Month)</td>  
                    </tr>
                    <tr>
                        <th>Facebook Friends</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Wall Comments</th>
                        <td>No</td>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Keywords Based Posting</th>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Monthly Wall Posting</th>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th class="divid" colspan="5">Twitter Marketing</th> 
                    </tr>
                    <tr>
                        <th>Twitter Page Creation</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Twitter Regular Updates</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Twitter Tweet Favorite</th>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Twitter Followers</th>
                        <td>No</td>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th class="divid" colspan="5">Linkedin Marketing</th> 
                    </tr>
                    <tr>
                        <th>Linkedin Account Management</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Linkedin Profile Creation</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Linkedin Regular Updates</th>
                        <td>No</td>
                        <td>No</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                </table>
            </div>
    <?php include '../layout/_footer.php'?>
<?php include '../layout/_end.php'?>
