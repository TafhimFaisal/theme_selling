<?php
$title = "Mobile Application service";
include '../layout/_header.php';
?>

    <?php include '../layout/navbar.php'?>

        
    <div class="container sarvicetable">
                <h1>
                    SEO Service pakage 
                </h1>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Basic</th>
                        <th>Bronze</th>
                        <th>Silver</th>
                        <th>Gold</th>
                        <th>Ploatinum</th>
                        <th>Extended</th>					
                    </tr>
                    <tr>
                        <th>SEO Package</th>
                        <td>15 Keywords</td>
                        <td>25 Keywords</td>
                        <td>40 Keywords</td>
                        <td>60 Keywords</td>
                        <td>90 Keywords</td>
                        <td>110 Keywords</td>
                    </tr>
                    <tr>
                        <th>Cost Per Month</th>
                        <td>$250</td>
                        <td>$390</td>
                        <td>$520</td>
                        <td>$750</td>
                        <td>$1000</td>
                        <td>$1,150</td>
                    </tr>
                    <tr>
                        <th class="divid" colspan="7">Reviews and Analysis</th> 
                    </tr>
                    <tr>
                        <th>Keywords Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Complete Website Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Critical SEO Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Back Link Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Comp0etitor Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Baseline Ranking Check</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Duplicate Content Check	</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th class="divid" colspan="7">On Page Search Engine Marketing</th> 
                    </tr>
                    
                    <tr>
                        <th>Meta Tags Optimization	</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Headding Tags Optimization <br>(H1, H2, H3)</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Anchor and Title Tags Optimization</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Image and Hyperlink Optimization</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Robots.txt Creation and Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>HTML Sitemap Creation Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>XML Sitemap Creation and Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Internal Linking Structuring</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Navigation Analysis</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Crawl Error Resolution</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Website Speed and Page Load time</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>W3c Validation Check</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Content Optimization</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Google Webmaster Account Setup</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th class="divid" colspan="7">On Page Search Engine Marketing</th> 
                    </tr>
                    <tr>
                        <th>Manual Search Engine Submission</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Article Writing (400+Words)</th>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>4</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Article Submission</th>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>4</td>
                        <td>24</td>  
                    </tr>
                    <tr>
                        <th>Business Listing</th>
                        <td>4</td>
                        <td>8</td>
                        <td>10</td>
                        <td>15</td>
                        <td>15</td>
                        <td>15</td>  
                    </tr>
                    <tr>
                        <th>Web 2.0 Profile Creation</th>
                        <td>2</td>
                        <td>3</td>
                        <td>3</td>
                        <td>4</td>
                        <td>6</td>
                        <td>8</td>  
                    </tr>
                    <tr>
                        <th>Blog Creation</th>
                        <td>1</td>
                        <td>2</td>
                        <td>2</td>
                        <td>3</td>
                        <td>4</td>
                        <td>6</td>  
                    </tr>
                    <tr>
                        <th>Blog Content Writing (300+ words)</th>
                        <td>1</td>
                        <td>3</td>
                        <td>3</td>
                        <td>4</td>
                        <td>4</td>
                        <td>6</td>  
                    </tr>
                    <tr>
                        <th>RSS Feed Submission</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th>Classifieds Submission</th>
                        <td>5</td>
                        <td>10</td>
                        <td>15</td>
                        <td>18</td>
                        <td>20</td>
                        <td>25</td>  
                    </tr>
                    <tr>
                        <th>Google Local Listing</th>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>
                        <td>Yes</td>  
                    </tr>
                    <tr>
                        <th class="divid" colspan="7">IMAGES & VIDEOS Optimizations (Image and Video will be provide by client)</th> 
                    </tr>
                    <tr>
                        <th>Image Optimization</th>
                        <td>1</td>
                        <td>4</td>
                        <td>5</td>
                        <td>5</td>
                        <td>8</td>
                        <td>10</td>  
                    </tr>
                    <tr>
                        <th>Slide Sharing</th>
                        <td>1</td>
                        <td>2</td>
                        <td>4</td>
                        <td>4</td>
                        <td>6</td>
                        <td>6</td>  
                    </tr>
                </table>
           </div>
    <?php include '../layout/_footer.php'?>
<?php include '../layout/_end.php'?>
