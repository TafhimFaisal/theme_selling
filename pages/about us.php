<?php 
    $title = "Wordpress";
    include '../layout/_header.php';
?>

    <?php include '../layout/navbar.php'?>
    <div class="container abourUs">
    
        <h1 class="firstheader">About us</h1>
        <p> 
            Our vision is to channel talented designer’s and developers’ hard work into a single global marketplace. Customers will find that themejunction.com is a one stop shop to make beautiful websites for their business clients leaving their website visitors with an everlasting impression. Our list of recommended plug-ins will help provide functionality as well as add beautiful modifications to your website makeing it unique and different from anyone else.
        </p>
            <h1 class="secondheader">
                Why ThemeJunction
            </h1>
            <p>
                ThemeJunction.com provides an effective way to save costs by skipping the process of developing a website from scratch. Instead we provide beautiful and innovative website templates which can be used with minimum modifications. We ensure you that the products in ThemeJunction.com are uploaded so that the customer can have the website of their dreams using minimal funds while simplifying the process of building and designing the website. 
                <br>
                <br>
                Receive appropriate documentation for your purchased products and have access to unlimited updates for them. If there are issues with any purchased product, get assistance from support free of charge from ThemeJunction.com. We are always here for you.
                <br>
                <br>
                Our dedicated Quality Assurance team ensures that only the best products areuploaded to our website. We make sure that only the top industry practices areimplemented on all website templates, quality pictures and meaningful colors are usedappropriately, and it is completely responsive to all major devices so that our customerscan benefit from them.
                <br>
                <br>
                Do you lack the skills to create or modify a template or theme for your website? We offer a complete 360-degree solution for you. Just choose your favorite WebsiteTemplate, provide us with your content and we will handle the rest. 
                <br>
                <br>
                We can provide marketing consultation and execute various SEO strategies for you asper your requirement.
                <br>
                <br>
                Utilize our highly skilled graphical engineers to contribute to your website and make it 
                even more luxurious and rich. 
                <br>
                <br>
                You can use the plug-ins to make your website more attractive and richer as well as usePlug-ins that provide functional services such as live chat, comments sections, analyticsand much more. 
                <br>
                <br>
                Our dedication to data security is second to none. This is why we applied SSL Securityor Https certificates to ThemeJunction.com, making your website visit as secured aspossible. This is implemented so that any session between website visitors andThemeJunction.com is encrypted and visitors can safely use their credit card andpersonal information anytime. 
        </p>
    </div>  
    <?php include '../layout/_footer.php'?>
<?php include '../layout/_end.php'?>
