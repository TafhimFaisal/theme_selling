<?php
    $title = "home"; 
    include 'layout/_header.php';
?>

    <?php include 'layout/navbar.php'?>

    <div class="slider_wrapper">
        <div class="slider_content container center_everything flax-direction-colmn">
            <div class="Slider_header center_everything flax-direction-colmn">
                <p>we are</p>
                <h1> <span class="Sliderjunction"> JUNCTION </span>THEMES</h1>
                <p>We made Premium themes for WordPress, OpenCart, Magento and PrestaShop</p>
            </div>
            <div class="sliderButton center_everything flax-direction-row">
                <a href="#" class="center_everything flax-direction-row">OUR THEMES</a>
                <a href="#" class="center_everything flax-direction-row">TAKE A TOUR</a>
            </div>

        </div>
    </div>

    <div class="Number_of_product_wrapper">
        <div class="Number_of_product container center_everything space-between">
            <div class="PREMIUM_THEMES center_everything flax-direction-colmn">
                <h1>
                    20
                </h1>
                <p>
                    PREMIUM THEMES
                </p>
            </div>            
            <div class="HAPPY_CUSTOMERS center_everything flax-direction-colmn">
                <h1>
                    1100
                </h1>
                <p>
                    HAPPY CUSTOMERS
                </p>
            </div>            
            <div class="TICKETS_SOLVED center_everything flax-direction-colmn">
                <h1>
                    250
                </h1>
                <p>
                    TICKETS SOLVED
                </p>
            </div>            
            <div class="SUPPORT_UPDATE center_everything flax-direction-colmn">
                <h1>
                    230
                </h1>
                <p>
                    SUPPORT &#38; UPDATE
                </p>
            </div>            
        </div>
    </div>

    <div class="description_one_article_wrapper">
        <article class="description_one container center_everything flax-direction-colmn">
            <h1>
                Premium WordPress, WooCommerce, OpenCart and Magento
                themes
            </h1>
            <h2>
                that will sell your products and services for you!
            </h2>
            <p class="">
                We create our themes to help your 
                business grow, no matter who you are 
                blogger or businessman - your website 
                will work for you because we added to 
                our themes all you need for success in 
                today’s IT world. All our themes very 
                easy in use and customization and provide 
                great User Experience for you and your site 
                visitors, you can setup your site in several 
                minutes and run your own business just for 
                price of one theme.  
            </p>
            <div class="description_one_article_button center_everything space-evenly">
                <a href="#">VIEW ALL THEMES</a>
                <a href="#">LEARN MORE</a>
            </div>
            <img src="img/pc.png" alt="" srcset="">
        </article>
    </div>

    <div class="description_two_article_wrapper">
        <div class="description_two container center_everything">
            <article>
                Easy guide “How to start WordPress Blog in 5 minutes” - <a href="#"> Learn now </a>
            </article>
        </div>
    </div>

    <div class="description_three_article_wrapper">
        <div class="description_three_article container center_everything flax-direction-colmn">
            <h1>
                <b>What you get</b> with every theme
                All of our themes are created with the following <b>important principles</b>
            </h1>
            <article>
                <div>
                    <h4>
                        WordPress best practices
                    </h4>
                    <p>
                        For third-party WordPress plugins compatibility 
                        and to ensure that our themes will work with future 
                        versions of WordPress without any problems and bugs 
                        free                        
                    </p>
                </div>
                <div>
                    <h4>
                        Lifetime Theme Updates
                    </h4>
                    <p>
                        We provide lifetime theme updates for FREE. 
                        You can be sure that you will always have the 
                        latest theme version and will receive new theme 
                        features and bug fixes for FREE.                        
                    </p>
                </div>
                <div>
                    <h4>
                        Quick setup with Sample Data
                    </h4>
                    <p>
                        2 minutes to setup theme and launch your site. 
                        We always provide Sample Demo data that you can 
                        import and get your site looks exactly like our 
                        demo site within 1 click!                        
                    </p>
                </div>
                <div>
                    <h4>
                        Easy to use for everyone
                    </h4>
                    <p>
                        You are just getting started or you are IT geek 
                        that can code with closed eyes? No matter what 
                        computer skills your have, all our themes have 
                        intuitive admin panel interface and everybody can 
                        start using it in several seconds.                        
                    </p>
                </div>
                <div>
                    <h4>
                        Modern professional designes
                    </h4>
                    <p>
                        We do our best to provide best quality 
                        modern designes with all our themes. 
                        Your visitors will not forget your creative 
                        stunning site design for years!                       
                    </p>
                </div>
                <div>
                    <h4>
                        Dedicated Premium Support
                    </h4>
                    <p>
                        All our themes comes with dedicated support. 
                        We have FAQ and special support ticket system 
                        with support team fast response time so you can 
                        be sure that you got answer for your question in 
                        time.                        
                    </p>
                </div>
            </article>
        </div>
    </div>

    <?php include 'layout/theme_list.php'?>

    <div class="reviews_wrapper center_everything">
        <div class="container center_everything flax-direction-colmn">

            <h1>
                What customers say about us
            </h1>
            <div class="reviews">
                <div>
                    <div class="img">
                        <img src="img/rivewer_imge.png" alt="" srcset="">
                    </div>
                    <p>
                        he theme is lovely itself, 
                        though we got great amount of help 
                        in customizing it the way we want. 
                        Theme Junction team was very helpful 
                        and answered al of our questions in a 
                        quite timely manner. <br>
                        <br>
                        John Herry<br> 
                        Founder and Managing Director - <span class="Cliant_name"> Jeweljerg </span>
                    </p> 
                </div>
                <div>
                    <div class="img">
                        <img src="img/rivewer_imge.png" alt="" srcset="">
                    </div>
                    <p>
                        he theme is lovely itself, 
                        though we got great amount of help 
                        in customizing it the way we want. 
                        Theme Junction team was very helpful 
                        and answered al of our questions in a 
                        quite timely manner. <br>
                        <br>
                        John Herry<br> 
                        Founder and Managing Director - <span class="Cliant_name"> Jeweljerg </span>
                    </p> 
                </div>
                <div>
                    <div class="img">
                        <img src="img/rivewer_imge.png" alt="" srcset="">
                    </div>
                    <p>
                        he theme is lovely itself, 
                        though we got great amount of help 
                        in customizing it the way we want. 
                        Theme Junction team was very helpful 
                        and answered al of our questions in a 
                        quite timely manner. <br>
                        <br>
                        John Herry<br> 
                        Founder and Managing Director - <span class="Cliant_name"> Jeweljerg </span>
                    </p> 
                </div>
                <div>
                    <div class="img">
                        <img src="img/rivewer_imge.png" alt="" srcset="">
                    </div>
                    <p>
                        he theme is lovely itself, 
                        though we got great amount of help 
                        in customizing it the way we want. 
                        Theme Junction team was very helpful 
                        and answered al of our questions in a 
                        quite timely manner. <br>
                        <br>
                        John Herry<br> 
                        Founder and Managing Director - <span class="Cliant_name"> Jeweljerg </span>
                    </p> 
                </div>
            </div>
        </div>
    </div>
    <div class="newsFeed_wrapper center_everything flax-direction-colmn">
        <h1>
            Theme updates and latest news
        </h1>
        <div class="newsFeed">
            <div>
                <img src="img/themelatst.png" alt="" srcset="">
                <p>
                    We reduced themes prices for our most 
                    popular themes
                    <br><br>
                    <span class="date">November 20, 2016</span>
                </p>
            </div>
            <div>
                <img src="img/themelatst.png" alt="" srcset="">
                <p>
                    We reduced themes prices for our most 
                    popular themes
                    <br><br>
                    <span class="date">November 20, 2016</span>
                </p>
            </div>
            <div>
                <img src="img/themelatst.png" alt="" srcset="">
                <p>
                    We reduced themes prices for our most 
                    popular themes
                    <br><br>
                    <span class="date">November 20, 2016</span>
                </p>
            </div>
            <div>
                <img src="img/themelatst.png" alt="" srcset="">
                <p>
                    We reduced themes prices for our most 
                    popular themes
                    <br><br>
                    <span class="date">November 20, 2016</span>
                </p>
            </div>
            <div>
                <img src="img/themelatst.png" alt="" srcset="">
                <p>
                    We reduced themes prices for our most 
                    popular themes
                    <br><br>
                    <span class="date">November 20, 2016</span>
                </p>
            </div>
            
        </div>
    </div>
    
    <div class="tags_wrapper center_everything">
        <p>
            #Themejunction - Our new #premium #WordPress #Personal &#38; Blog theme just released on #Themeforest- <br>
            themeforest.net/item/themejunction <br>
            <svg height="3" width="279" >
            <line x1="0" y1="0" x2="279" y2="0" style="stroke:#008FD0;stroke-width:3" />
            <br>
            @themejunction 2016
        </p>
    </div>
    
    <?php include 'layout/_footer.php'?>

<?php include 'layout/_end.php'?>
